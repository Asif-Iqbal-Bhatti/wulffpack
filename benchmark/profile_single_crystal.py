from wulffpack import SingleCrystal  # NOQA
import cProfile

surface_energies = {(1, 1, 1): 1.0,
                    (1, 0, 0): 1.1,
                    (1, 1, 0): 1.1,
                    (2, 1, 1): 1.1,
                    (3, 2, 1): 1.1,
                    (5, 1, 1): 1.1,
                    (5, 4, 0): 1.1,
                    (5, 3, 0): 1.1,
                    (5, 2, 0): 1.1,
                    (5, 1, 0): 1.1,
                    }

cProfile.run('SingleCrystal(surface_energies)', sort='cumtime')
