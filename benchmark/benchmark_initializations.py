from wulffpack import (SingleCrystal,
                       Decahedron,
                       Icosahedron,
                       Winterbottom)
import time


surface_energies = {(1, 1, 1): 1.0,
                    (1, 0, 0): 1.1,
                    (1, 1, 0): 1.1,
                    (2, 1, 1): 1.1,
                    (3, 2, 1): 1.1,
                    (5, 1, 1): 1.1,
                    (5, 4, 0): 1.1,
                    (5, 3, 0): 1.1,
                    (5, 2, 0): 1.1,
                    (5, 1, 0): 1.1,
                    }
twin_energy = 0.05
interface_direction = (3, 2, 2)
interface_energy = 0.5

start = time.time()
SingleCrystal(surface_energies)
end = time.time()
print('Time SingleCrystal: {:.5} s'.format(end - start))

start = time.time()
Decahedron(surface_energies,
           twin_energy=twin_energy)
end = time.time()
print('Time Decahedron: {:.5} s'.format(end - start))

start = time.time()
Icosahedron(surface_energies,
            twin_energy=twin_energy)
end = time.time()
print('Time Icosahedron: {:.5} s'.format(end - start))

start = time.time()
Winterbottom(surface_energies,
             interface_direction=interface_direction,
             interface_energy=interface_energy)
end = time.time()
print('Time Winterbottom: {:.5} s'.format(end - start))
