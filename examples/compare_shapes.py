from wulffpack import (SingleCrystal,
                       Decahedron,
                       Icosahedron)
from ase.build import bulk
import numpy as np

# Specify parameters
prim = bulk('Au',
            crystalstructure='fcc',
            a=4.08)
surface_energies = {(1, 0, 0): 0.066,
                    (1, 1, 1): 0.06}
twin_energy = 1e-3
natoms = 1000

# Create one particle per type
oh = SingleCrystal(surface_energies,
                   primitive_structure=prim,
                   natoms=natoms)
dh = Decahedron(surface_energies,
                twin_energy=twin_energy,
                primitive_structure=prim,
                natoms=natoms)
ih = Icosahedron(surface_energies,
                 twin_energy=twin_energy,
                 primitive_structure=prim,
                 natoms=natoms)

# Compare volumes
print('Volume (sanity check):')
for name, particle in zip(['Oh', 'Dh', 'Ih'], [oh, dh, ih]):
    volume = particle.volume
    print('{}: {:.2f}'.format(name, volume))
print()

# Compare areas
print('Total surface area (excluding twin boundaries):')
for name, particle in zip(['Oh', 'Dh', 'Ih'], [oh, dh, ih]):
    area = particle.area
    print('{}: {:.2f}'.format(name, area))
print()

# Compare fractions of {100} facets
print('Fraction of {100} facets:')
for name, particle in zip(['Oh', 'Dh', 'Ih'], [oh, dh, ih]):
    fraction = particle.facet_fractions.get((1, 0, 0), 0.)
    print('{}: {:.4f}'.format(name, fraction))
print()

# Compare total surface energies
print('Total surface energy (including twin energy):')
for name, particle in zip(['Oh', 'Dh', 'Ih'], [oh, dh, ih]):
    energy = particle.surface_energy
    print('{}: {:.2f}'.format(name, energy))
print()

# Compare average surface energies
print('Average surface energy (excluding twin energy):')
for name, particle in zip(['Oh', 'Dh', 'Ih'], [oh, dh, ih]):
    energy = particle.average_surface_energy
    print('{}: {:.4f}'.format(name, energy))
print()

# Compare strain energies
shear_modulus = 0.17  # eV / Angstrom^3
poissons_ratio = 0.42  # unitless
print('Estimated strain energies:')
for name, particle in zip(['Dh', 'Ih'], [dh, ih]):
    energy = particle.get_strain_energy(shear_modulus=shear_modulus,
                                        poissons_ratio=poissons_ratio)
    print('{}: {:.2f}'.format(name, energy))
print()

# Compare edge length and number of corners
print('Number of corners and total edge length:')
for name, particle in zip(['Oh', 'Dh', 'Ih'], [oh, dh, ih]):
    ncorners = particle.number_of_corners
    edge_length = particle.edge_length
    print('{}: {} corners, edge length: {:.2f}'.format(name,
                                                       ncorners,
                                                       edge_length))
