.. raw:: html

  <p>
    <a href="https://badge.fury.io/py/wulffpack"><img alt="pypi badge" src="https://badge.fury.io/py/wulffpack.svg"/></a>
    <a style="border-width:0" href="https://doi.org/10.21105/joss.01944">
      <img src="https://joss.theoj.org/papers/10.21105/joss.01944/status.svg" alt="DOI badge" >
    </a>
  </p>

WulffPack – a package for Wulff constructions
=============================================

:program:`WulffPack` is a Python package for making Wulff constructions,
typically for finding equilibrium shapes of nanoparticles. WulffPack constructs
both continuum models and atomistic structures for further modeling with, e.g.,
molecular dynamics or density functional theory.

.. code-block:: python
   
    from wulffpack import SingleCrystal
    from ase.io import write
    surface_energies = {(1, 1, 1): 1.0, (1, 0, 0): 1.2}
    particle = SingleCrystal(surface_energies)
    particle.view()
    write('atoms.xyz', particle)

:program:`WulffPack` constructs the regular, single-crystalline Wulff shape
as well as decahedra, icosahedra, and particles in contact with a flat
surface (the Winterbottom construction). Any crystal symmetry can be handled.
Resulting shapes are conveniently visualized with
`Matplotlib <https://matplotlib.org/>`_.

.. figure:: _static/particles.png
   :alt: Three particles

   Three equilibrium shapes created by :program:`WulffPack`: truncated octahedron (left),
   truncated decahedron (middle), and truncated icosahedron (right). The
   figure was created with the code in :ref:`this example <subplots>`.

:program:`WulffPack` has been developed at the Department of Physics of
Chalmers University of Technology in Gothenburg, Sweden. :program:`WulffPack`
and its development are hosted on `gitlab
<https://gitlab.com/materials-modeling/wulffpack>`_.  Bugs and feature
requests are ideally submitted via the `gitlab issue tracker
<https://gitlab.com/materials-modeling/wulffpack/issues>`_.  The
development team can also be reached by email via
wulffpack@materialsmodeling.org.

Wulff constructions in a web application
----------------------------------------

:program:`WulffPack` provides the backbone of 
`a web application in SHARC
<https://sharc.materialsmodeling.org/wulff_construction>`_,
in which Wulff constructions for cubic crystals can be created easily.


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   background
   examples/index
   moduleref/index
   bibliography
   credits

Indices and tables
------------------

* :ref:`genindex`
* :ref:`search`

.. figure:: _static/winterbottom_blender.jpg
    :alt: Winterbottom construction visualized with Blender
    
    A :class:`Winterbottom construction <wulffpack.Winterbottom>` visualized
    with `Blender <https://docs.blender.org>`__.
