Winterbottom construction
=========================

.. module:: wulffpack

.. index::
   single: Class reference; Winterbottom

.. autoclass:: Winterbottom
    :members:
    :undoc-members:
    :inherited-members:

