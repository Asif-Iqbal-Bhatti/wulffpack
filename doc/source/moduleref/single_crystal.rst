Single crystalline particle
===========================

.. module:: wulffpack

.. index::
   single: Class reference; SingleCrystal

.. autoclass:: SingleCrystal
    :members:
    :undoc-members:
    :inherited-members:
