.. _credits:
.. index:: Credits

Credits
*******

**WulffPack** has been developed at Chalmers University of Technology in
Gothenburg (Sweden) in the `Condensed Matter and Materials Theory division
<http://www.materialsmodeling.org>`_ at the Department of Physics.

When using :program:`WulffPack` in your research please cite:

| J. M. Rahm and P. Erhart
| *WulffPack: A Python package for Wulff constructions*
| J. Open Source Softw. **5**, 1944 (2020)
| `doi: 10.21105/joss.01944 <https://doi.org/10.21105/joss.01944>`_
