.. _examples_graphics:
.. highlight:: python
.. index::
   single: Examples; Graphics

Graphics
========

:program:`WulffPack` offers several means to visualize the created particles.
This page collects some examples.

Show a particle with matplotlib
-------------------------------

The simplest way to show a particle with :program:`WulffPack` is with the
:func:`view <wulffpack.core.BaseParticle.view>` function. The function uses
`matplotlib <https://matplotlib.org/>`_ and the user can specify colors of
facets, transparency and thickness of the lines between the facets via optional
keywords. The keyword ``save_as`` specifies the filename to save the figure as.
If none is specified, the particle is shown with the matplotlib GUI.

.. literalinclude:: ../../../examples/graphics.py
   :start-after: # Define energies
   :end-before: # Use continuous

.. figure:: ../_static/single_particle.png
   :alt: Single particle

If the particle contains vicinal surfaces, it can be advantageous to
highlight the vicinality between surfaces with a continuous color scheme,
such that surfaces that are similar have similar colors.
:program:`WulffPack` has a function
:func:`get_continuous_color_scheme
<wulffpack.core.BaseParticle.get_continuous_color_scheme>`, which creates
such color schemes automatically. Note that these color schemes are
sensible primarily for crystals with cubic symmetry.

.. literalinclude:: ../../../examples/graphics.py
   :start-after: # Use continuous
   :end-before: # Make figure with subplots

.. figure:: ../_static/vicinal_particle.png
   :alt: Single particle

.. _subplots:

Make subplots
-------------

For more control over the matplotlib figure, one can also get axis objects
directly. In the following snippet, a truncated octahedron, decahedron and
icosahedron are created and put as subplots in the same figure.

Note that it can be quite tricky to get 1:1 aspect ratio when plotting 3D with
matplotlib. In this example, the figure size is manually set to three times as
wide as tall, and the distance from edge of the window to plot as well as
between plots is set to zero using `subplots_adjust()`. This ensures that the
plots are not squeezed in any direction.

.. literalinclude:: ../../../examples/graphics.py
   :start-after: # Make figure with subplots
   :end-before: # Use ase.visualize.view

.. figure:: ../_static/particles.png
   :alt: Three particles

Visualize atoms objects
-----------------------

The possibility to extract an `ASE Atoms
<https://wiki.fysik.dtu.dk/ase/ase/atoms.html>`_ object opens an arsenal of
visualization opportunities. The arguably simplest one is ASE's GUI.

.. literalinclude:: ../../../examples/graphics.py
   :start-after: # Use ase.visualize.view
   :end-before: # Write atoms to file

.. figure:: ../_static/particle_atoms.jpg
   :alt: Atomistic structure

   A truncated icosahedron visualized with ASE's GUI.

Since an `ASE Atoms <https://wiki.fysik.dtu.dk/ase/ase/atoms.html>`_ object can
be `written to file
<https://wiki.fysik.dtu.dk/ase/ase/io/io.html#ase.io.write>`_, virtually any
software that visualizes atomistic structures can be utilized.

.. literalinclude:: ../../../examples/graphics.py
   :start-after: # Write atoms to file
   :end-before: # Write wavefront obj file

.. figure:: ../_static/particle_ovito.png
   :alt: Atomistic structure from ovito

   A truncated icosahedron visualized with `OVITO <https://ovito.org/>`_.

Use wavefront .obj files
------------------------

The :func:`write <wulffpack.core.BaseParticle.write>` can write particles to
`Wavefront .obj <https://en.wikipedia.org/wiki/Wavefront_.obj_file>`_ files.
Such files are useful in many 3D software, such as `Blender
<https://docs.blender.org/manual/en/2.80/addons/io_scene_obj.html>`_.

.. literalinclude:: ../../../examples/graphics.py
   :start-after: # Write wavefront obj file

.. figure:: ../_static/winterbottom_blender.jpg
    :alt: Winterbottom construction visualized with Blender
    
    A :class:`Winterbottom construction <wulffpack.Winterbottom>` visualized
    with `Blender <https://docs.blender.org>`__.

Source code
-----------

.. container:: toggle

    .. container:: header

       The complete source code is available in
       ``examples/graphics.py``

    .. literalinclude:: ../../../examples/graphics.py
