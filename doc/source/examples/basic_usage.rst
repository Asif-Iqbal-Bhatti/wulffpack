.. _examples_basic_usage:
.. highlight:: python
.. index::
   single: Examples; Basic usage

Basic usage
===========

The simplest usages of :program:`WulffPack` are illustrated here. Please note
that the surface energies used in the examples are arbitrary.

Modules to import
-----------------

To run the following examples, three :program:`WulffPack` classes are imported
(:class:`SingleCrystal <wulffpack.SingleCrystal>`, :class:`Decahedron
<wulffpack.Decahedron>`, and :class:`Icosahedron <wulffpack.Icosahedron>`). In
addition, two functions from `ASE <https://wiki.fysik.dtu.dk/ase>`_ will be
used, :func:`ase.build.bulk` and :func:`ase.io.write`.

.. literalinclude:: ../../../examples/basic_usage.py
   :end-before: # Show a regular

Regular Wulff construction
--------------------------

In the simplest case, the only input :program:`WulffPack` needs to make a
Wulff construction is one or several surface energies. Surface energies are
specified as a dictionary, where the keys are the Miller indices as a tuple
and the values are surface energies as floats.

:program:`WulffPack` does not impose any units on surface energies but uses
only their ratio to determine the shape. This means that the surface energies
can be specified in any units of choice, such as J/m^2 or eV/Å^2. Note,
however, that ASE, which is used to define the primitive structure in
:program:`WulffPack`, uses Ångström, meaning that eV/Å^2 is recommended to
obtain a sensible total surface energy from the Wulff construction.

.. literalinclude:: ../../../examples/basic_usage.py
   :start-after: # Show a regular
   :end-before: # Wulff construction for hcp

The last two lines show a rendition of the particle with the aid of
`matplotlib <https://matplotlib.org/>`_ and save an xyz file with an atomistic
version of the structure. By default, the particle will be carved out from an
FCC crystal and have roughly 1000 atoms. The crystal structure and the number
of atoms can both be modified, as demonstrated below.

Regular Wulff construction for an HCP crystal
---------------------------------------------

:program:`WulffPack` allows Wulff constructions based on arbitrary crystals.
To use a crystal other than FCC, one specifies a primitive structure in the
form of an `ASE Atoms <https://wiki.fysik.dtu.dk/ase/ase/atoms.html>`_ object.
In the below example, a Wulff construction based on an hexagonal close-packed
(HCP) crystal is created. Bravais-Miller indices are used to denote facets.

.. literalinclude:: ../../../examples/basic_usage.py
   :start-after: # Wulff construction for hcp
   :end-before: # Change number of atoms

.. note::

  :program:`WulffPack` uses `Spglib <https://atztogo.github.io/spglib/>`_ to
  standardize the input primitive structure. The Miller (or Bravais-Miller)
  indices should always refer to this standardized version of the input
  primitive structure. In the case of FCC, for example, the Miller indices
  should refer to the conventional, cubic cell even if :program:`WulffPack`
  is provided with a primitive FCC cell. The conventions
  for the standardization are detailed in the `Spglib documentation
  <https://atztogo.github.io/spglib/definition.html#def-idealize-cell>`_. If
  unsure, the standardized cell is a property of the :class:`SingleCrystal
  <wulffpack.SingleCrystal>` object:

  .. code::

    from ase.visualize import view
    view(particle.standardized_structure)

  The Miller indices should thus always refer to the reciprocal cell 
  corresponding to the cell of that standardized structure.

If a larger atoms object with the same shape is wanted, the quickest solution
is to simply set the ``natoms`` attribute of the same particle.

.. literalinclude:: ../../../examples/basic_usage.py
   :start-after: # Change number of atoms
   :end-before: # Wulff construction for decahedron

By doing so, all the coordinates are scaled such that the volume matches the
new number of atoms.

Wulff construction of twinned particles
---------------------------------------

:program:`WulffPack` can also construct decahedral and icosahedral particles
using the so-called modified Wulff construction [Mar83]_. In this case a cubic
crystal is required, and FCC is probably the only crystal structure that makes
physical sense. For this construction, it is also mandatory to specify a twin
boundary energy.

.. literalinclude:: ../../../examples/basic_usage.py
   :start-after: # Wulff construction for decahedron
   :end-before: # Wulff construction for icosahedron

The interface to construct an icosahedral particle is identical.

.. literalinclude:: ../../../examples/basic_usage.py
   :start-after: # Wulff construction for icosahedron

Source code
-----------

.. container:: toggle

    .. container:: header

       The complete source code is available in
       ``examples/basic_usage.py``

    .. literalinclude:: ../../../examples/basic_usage.py



