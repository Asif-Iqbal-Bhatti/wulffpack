.. _examples_compare_shapes:
.. highlight:: python
.. index::
   single: Examples; Compare shapes

Compare shapes
==============

It is sometimes of interest to compare nanoparticles of different structural
motifs. This example illustrates how truncated octahedral, decahedral, and
icosahedral particles constructed based on the same surface energies can be
compared with :program:`WulffPack`.

Preparations
------------

Before creating the particles, some parameters should be chosen (the values
here are arbitrary). For simplicity, we will only consider {111} and {100}
facets, but :program:`WulffPack` can handle an arbitrary number of facets
(although the code may take a fairly long time to run). When doing
quantitative analyses of the particles, it is recommended to use electronvolts
as unit for energy and Ångström as unit for distance.

.. literalinclude:: ../../../examples/compare_shapes.py
   :end-before: # Create one particle per type

We then create three particles, one of every kind.

.. literalinclude:: ../../../examples/compare_shapes.py
   :start-after: # Create one particle per type
   :end-before: # Compare volumes

Volume
------

We may now compare the three particles. For peace of mind, we first check that
they have the same volume. In :program:`WulffPack`, the volume is determined
from the specified number of atoms and the primitive structure together. It
does not take into account that the specified number of atoms may be
inadequate to create the specified shape.

.. literalinclude:: ../../../examples/compare_shapes.py
   :start-after: # Compare volumes
   :end-before: # Compare areas

This snippet produces the following output:

::

  Volume (sanity check):
  Oh: 16979.33
  Dh: 16979.33
  Ih: 16979.33

Area
----

We may now turn to something more interesting: the surface area. The syntax is
the same as for the volume:

.. literalinclude:: ../../../examples/compare_shapes.py
   :start-after: # Compare areas
   :end-before: # Compare fractions

::

  Total surface area (excluding twin boundaries):
  Oh: 3488.01
  Dh: 3468.81
  Ih: 3400.92

As one might have expected, the surface area of the nearly spherical
icosahedron is the smallest.

Fraction of a specified facet
-----------------------------

It can also be of interest to compare how large fraction of the area that is,
say, {100}:

.. literalinclude:: ../../../examples/compare_shapes.py
   :start-after: # Compare fractions
   :end-before: # Compare total surface energies

::

  Fraction of {100} facets:
  Oh: 0.2775
  Dh: 0.2373
  Ih: 0.0000

The truncated octahedron has the largest fraction of {100} facets, which is
the primary reason decahedra and icosahedra are often energetically favorable
for small nanoparticles made of FCC metals, for which the {111} energy is
typically lower than {100}.

Surface energy
--------------

We may compare the total surface energy of the three particles, this
time including the energy of the twin boundaries in the decahedral and
icosahedral particles:

.. literalinclude:: ../../../examples/compare_shapes.py
   :start-after: # Compare total surface energies
   :end-before: # Compare average surface energies

::

  Total surface energy (including twin energy):
  Oh: 215.09
  Dh: 215.07
  Ih: 208.82

The output indicates that the icosahedron is the most stable shape, followed
by the decahedron. Note, however, that this model only includes surface
energies (including twin boundary energies) and no contributions from, e.g.,
strain (see below).

Average surface energy
----------------------

It is sometimes of interest to extract the average surface energy
of the particle, i.e. the average of the surface energy per facet :math:`E_f`
weighted with the area fractions of each facet :math:`f`,

.. math::

  E_\text{average} = \frac{1}{A_\text{tot}} \sum_f A_f E_f,

where :math:`A_f` is the area of facet :math:`f` and :math:`A_\text{tot}` is
the total area of the particle.

.. literalinclude:: ../../../examples/compare_shapes.py
   :start-after: # Compare average surface energies
   :end-before: # Compare strain energies

::

  Average surface energy (excluding twin energy):
  Oh: 0.0617
  Dh: 0.0614
  Ih: 0.0600

This quantity is of particular interest in the single crystalline (Oh) case,
since it provides an estimate for the surface energy that would be measured
experimentally.

Strain energy
-------------

Decahedral and icosahedral particles can be built from FCC crystalline grain
only if they are strained. This strain energy can be estimated based on
continuum mechanics if we know the shear modulus and Poisson's ratio of the
material [HowMar84]_.

.. literalinclude:: ../../../examples/compare_shapes.py
   :start-after: # Compare strain energies
   :end-before: # Compare edge length and number of corners

::

  Estimated strain energies:
  Dh: 0.52
  Ih: 5.94

We note that for this size, the strain energy is sufficient to make the
decahedron less stable than the truncated octahedron (the sum of total surface
energy and strain energy is larger for the decahedron). Note that these strain
energies should only be taken as fairly crude approximations.

Number of corners and total edge length
---------------------------------------

Finally, it can be interesting to compare how many corners (vertices) the
particles have, as well as the total length of their edges. Note that these
only includes corners and edges on the surface of the particle, not the 1D or
2D defects inside decahedra and icosahedra.

.. literalinclude:: ../../../examples/compare_shapes.py
   :start-after: # Compare edge length and number of corners

::

  Number of corners and total edge length:
  Oh: 24 corners, edge length: 409.48
  Dh: 42 corners, edge length: 610.03
  Ih: 72 corners, edge length: 614.70

Note that for both the decahedron and the icosahedron, there are very small
"reentrant" surfaces close to where the grains meet at the fivefold axis.
These reentrant surfaces increase the number of corners significantly, but are
unlikely to manifest in an atomistic representation of these shapes. One can
in principle remove these facets by specifying a very small value for the twin
energy.

Source code
-----------

.. container:: toggle

    .. container:: header

       The complete source code is available in
       ``examples/compare_shapes.py``

    .. literalinclude:: ../../../examples/compare_shapes.py



