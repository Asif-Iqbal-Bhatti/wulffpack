image: python:3.8

include:
  - template: Code-Quality.gitlab-ci.yml

build:
  stage: build
  script: 
    - INSTDIR=$PWD/opt/lib/python
    - mkdir -p $INSTDIR
    - export PYTHONPATH=$INSTDIR:$PYTHONPATH
    - python3 ./setup.py install --home=$PWD/opt
    - echo "import site; site.addsitedir('${INSTDIR}')" > $INSTDIR/sitecustomize.py
  artifacts:
    paths:
      - opt/
    expire_in: 14 days


test:
  stage: test
  dependencies:
    - build
  tags:
    - linux
  script:
    - python -m pip install coverage
    - export PATH=${PATH}:${HOME}/.local/bin
    # Python path used throughout
    - INSTDIR=$PWD/opt/lib/python
    - export PYTHONPATH=$INSTDIR:$PYTHONPATH
    # run tests via coverage
    - coverage run tests/main.py
    - coverage report -m
    # generate html version of detailed coverage report
    - coverage html
  coverage: '/TOTAL.+ ([0-9]{1,3}%)/'
  artifacts:
    paths:
      - htmlcov
    expire_in: 14 days
  
style_check:
  stage: test
  tags:
    - linux
  script:
    - python3 tests/check_flake8__.py
  allow_failure: true 
    

code_quality:
  artifacts:
    paths: [gl-code-quality-report.json]

pages:
  stage: deploy
  dependencies:
    - build
    - test
  script:
    # prepare homepage
    - mkdir -p public/dev
    # code coverage report
    - cp -dr htmlcov/ public/coverage/
    # build user guide
    - INSTDIR=$PWD/opt/lib/python
    - export PYTHONPATH=$INSTDIR:$PYTHONPATH
    - python3 -m pip install cloud_sptheme
    - python3 -m pip install sphinx_autodoc_typehints
    - python3 -m pip install sphinx-sitemap
    - python3 -m pip install sphinx_rtd_theme
    - python3 -m pip install --upgrade jinja2==3.0.3


    # STABLE VERSION
    - tag=$(git tag | tail -1)
    - echo "tag= $tag"
    - git checkout $tag
    - sphinx-build doc/source/ public/

    # make tests and examples downloadable
    - apt-get update
    - apt-get install -y zip unzip
    - find examples/ -print | zip public/examples.zip -@
    - find tests/ -print | zip public/tests.zip -@

    # DEVELOPMENT VERSION
    - git checkout master
    - tag=$(git describe | tail -1)
    - echo "tag= $tag"
    # build user guide
    - sed -i "s/version = ''/version = '$tag'/" doc/source/conf.py
    - grep version doc/source/conf.py
    - sphinx-build doc/source/ public/dev/

    # make tests and examples downloadable
    - find examples/ -print | zip public/dev/examples.zip -@
    - find tests/ -print | zip public/dev/tests.zip -@

    # clean up
    - ls -l public/
    - chmod go-rwX -R public/
  artifacts:
    paths:
      - public
  only:
    - master
    - tags


pypi:
  stage: deploy
  tags:
    - linux
  only:
    - tags
  when: manual
  environment:
      name: pypi-upload
  script:
    # install twine
    - python3 -m pip install twine
    # check out the latest tag (redundant if job is limited to tags; still a sensible precaution)
    - tag=$(git tag | tail -1)
    - echo "tag= $tag"
    - git checkout $tag
    # create source distribution and push to PyPI
    - python3 setup.py sdist
    - python3 setup.py bdist_wheel --universal
    - ls -l dist/
    - python3 -m twine upload dist/*
